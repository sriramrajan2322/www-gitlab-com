---
layout: handbook-page-toc
title: Crucial Conversations Training at GitLab
description: "An outline of the internal Crucial Conversations training structure and continuous learning opportunities"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


# Crucial Conversations

Two members of the GitLab Learning & Development Team are certified to deliver Crucial Conversations Training. You can learn more about the importance of Crucial Converations at GitLab on our [Crucial Conversations](/handbook/leadership/crucial-conversations/) handbook page. 

If you complete Crucial Conversations training with one of our in house certified trainers, you can utilize our [growth and development budget](/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/#crucial-conversations-training). 

## What to Expect 

The video below gives an overview of what the Crucial Conversations training looks like. 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/UX-ArZJJJ1U" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

A detailed training outline is included below:

**Total program length:** 4 Weeks
**Time commitment per week:** Approx 2 hours self-paced and 1 hour live practice session

| Week Number | Topic | Commitment |
| ----- | ----- | ----- |
| 1 | Get Unstuck, Master my Stories 1, and Master my Stories 2 | 3 hours |
| 2 | Start with Heart, State my Path | 3 hours |
| 3 | Make it Safe, Learn to Look, and Seek Mutual Purpose | 3 hours |
| 4 | Explore Other's Paths, Move to Action | 3 hours |

Training weeks will run Wednesday-Wednesday, with 1 live trainings hosted each week. Upon completion of the 4 week training, you will earn an official Crucical Conversations certification from Crucial Learning.

## Upcoming Sessions 

If you are a GitLab Team Member and interested in participating in the course, you can fill out our [Crucial Conversations interest form](https://docs.google.com/forms/d/e/1FAIpQLSdqwibbQZs-zL-IX9aq9Yzgozm-y3i0Vwh59T8T1nR74mxmFQ/viewform).

**FY22** 

- 2021-12: Security department course
- 2022-02: Begins January 26th, sign-ups closed
- FY23-Q2: Dates to be confirmed

### Setting up the Training 

1. Use the Crucial Conversations [Course Manager Tool](https://training.vitalsmarts.com/learn/sign_out?client=gitlab) and [Course Manager Guide].
1. Ensure we have a sign up link from crucial conversations. 
1. Once we have the link, send the invite email to all team members interested in the program. 
1. Add all cohort participants to the [#crucial-conversations](https://app.slack.com/client/T02592416/C0258087472) Slack channel
1. Add all participants to the live event calendar invites 
1. Create a subfolder in the [Crucial Conversations](https://drive.google.com/drive/u/1/folders/144sRv0ap4Gwp4IcM_mtkK83c4toVGJZJ?ths=true) Google Drive to organize cohort materials
1. Use the [live session template](https://docs.google.com/presentation/d/1cXLjK_9_7ndngmgW_5z4yKcLx7iCxZSNgvEVZ7fNJEs/edit?usp=sharing) to organize GitLab-customized Crucial Conversation scenarios for each live session

# Crucial Conversations Alumni

After you've completed the Crucial Conversations course at GitLab, there are a few ways you can continue to practice your skills:

1. Attend the **Crucial Conversations Alumni sync session**. This 25-minute call happens every other month starting in January 2022. Check the GitLab team member's calendar for the invitation
1. Star and check the **#crucial-conversations** [Slack channel](https://app.slack.com/client/T02592416/C0258087472/user_profile/UBE4ZBF1N)for async discussion every other month, starting in February 2022
1. Fork or copy [this GitLab project for practicing Crucial Conversations](https://gitlab.com/gitlab-com/people-group/learning-development/crucial-conversations). Use the issue templates in the project to create your own issues, set due dates, and pratice your skills after you complete the certification.


