---
layout: markdown_page
title: "Third Party Minimum Security Standards"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Third Party Minimum Security Standards

## Scope
In order to maintain the confidentiality, availability and integrity of GitLab [classified data](/handbook/engineering/security/data-classification-standard.html), this minimum security standards guide was developed. This guide is applicable to any Third Party with an **inherent risk score of Moderate or High** as determined by the first phase of the Third Party Risk Management procedure.

## Roles and Responsibilities

| Role | Responsibilities |
| ------ | ------ |
| **Security Risk Team** | Maintain Third Party Minimum Security Standards |
| | Review provided evidence as part of the Third Party Risk Management activities|
| | Communicate observations and advise in remediation |
| | Conduct additional risk-based reviews, as applicable |
| **Business Owner (requester)** | Participate in the initial Inherent Risk determination process|
| | Provide Minimum Standards guide to the Third Party |
|  | Act as liaison with Third Party and Security Risk team, as needed, to complete assessment |
| | At the end of the engagement, ensure all data is destroyed or returned |
| | Participate in periodic access reviews, as applicable |


## Standards

The following standards will be reviewed during the Third Party Risk Management procedure.

### Third Parties Providing Applications (Free or Paid)

- The third party must staff a function to centrally-govern cybersecurity and privacy controls.
- The third party must establish, maintain and disseminate cybersecurity and privacy policies, standards and procedures
- The third party must review cybersecurity and privacy policies, standards and procedures at planned intervals or when significant changes occur to ensure their continuing suitability, adequacy and effectiveness.
- The third party must facilitate the operation of physical and environmental protection controls.
- The third party must maintain a current list of personnel with authorized access to organizational facilities (except for those areas within the facility officially designated as publicly accessible).
- The third party must enforce physical access authorizations for all physical access points (including designated entry/exit points) to facilities (excluding those areas within the facility officially designated as publicly accessible).
- Third Party must provide a copy of an annual independent audit (SOC 2 Type II) and, if applicable, bridge letter.
- The third party must detect vulnerabilities and configuration errors by recurring vulnerability scanning of systems and web applications.
- The third party must conduct penetration testing on systems and web applications.
- The third party must facilitate the implementation of identification and access management controls.
- The third party must securely manage passwords for users and devices.
- The third party must utilize the concept of least privilege, allowing only authorized access to processes necessary to accomplish assigned tasks in accordance with organizational business functions.
- The third party must uniquely identify and authenticate devices before establishing a connection.
- The third party must require Multi-Factor Authentication (MFA) for remote network access.
- The third party must facilitate the implementation of enterprise-wide monitoring controls.
- The third party must utilize a Security Incident Event Manager (SIEM) or similar automated tool, to support the centralized collection of security-related event logs.
- The third party must configure systems to produce audit records that contain sufficient information to, at a minimum: 
    - Establish what type of event occurred; 
    - When (date and time) the event occurred; 
    - Where the event occurred; 
    - The source of the event; 
    - The outcome (success or failure) of the event; and 
    - The identity of any user/subject associated with the event?
- The third party must provide an event log report generation capability to aid in detecting and assessing anomalous activities.
- The third party must retain audit records for a time period consistent with records retention requirements to provide support for after-the-fact investigations of security incidents and to meet statutory, regulatory and contractual retention requirements.
- The third party must detect and respond to anomalous behavior that could indicate account compromise or other malicious activities.
- The third party must utilize antimalware technologies to detect and eradicate malicious code.
- The third party must utilize File Integrity Monitor (FIM) technology to detect and report unauthorized changes to system files and configurations.
- The third party must utilize Host-based Intrusion Detection / Prevention Systems (HIDS / HIPS) on sensitive systems.
- The third party must utilize cryptographic mechanisms to protect the confidentiality of data being transmitted.
- The third party must utilize cryptographic mechanisms on systems to prevent unauthorized disclosure of information at rest.
- The third party must maintain network architecture diagrams that:
    - Contain sufficient detail to assess the security of the network's architecture;
    - Reflect the current state of the network environment; and
    - Document all sensitive data flows?"
- The third party must employ mechanisms that:
    - Retain Personal Data (PD), including metadata, for an organization-defined time period to fulfill the purpose(s) identified in the notice or as required by law;
    - Disposes of, destroys, erases, and/or anonymizes the PI, regardless of the method of storage; and
    - Uses organization-defined techniques or methods to ensure secure deletion or destruction of PD (including originals, copies and archived records)?"
- The third party must maintain network architecture diagrams that:
    - Contains sufficient detail to assess the security of the network's architecture;
    - Reflects the current state of the network environment; and
    - Documents all sensitive data flows?"
- The third party must identify and document the critical systems, applications and services that support essential missions and business functions.
- The TPRM tester must obtain BitSight Report
- The TPRM tester must formally assess and ensure assessors or assessment teams have the appropriate independence to conduct cybersecurity and privacy security control assessments in systems, applications and services through Information Assurance Program (IAP) activities to determine the extent to which the security controls are implemented correctly, operating as intended and producing the desired outcome with respect to meeting expected requirements for:
    - Statutory, regulatory and contractual compliance obligations;
    - Monitoring capabilities;
    - Mobile devices;
    - Databases;
    - Application security;
    - Embedded technologies (e.g., IoT, OT, etc.);
    - Vulnerability management;
    - Malicious code;
    - Insider threats; and
    - Performance/load testing."
- The third party must manage personnel security risk by screening individuals prior to authorizing access.
- The third party must facilitate the implementation of security workforce development and awareness controls?
- The third party must require internal and third-party users to sign appropriate access agreements prior to being granted access?
- For SOC scoped systems only, the TPRM tester must review the SOC report for CUECs and use the import template to map them to GitLab controls.

This information is collected by sending an automated questionnaire from ZenGRC. This questionnaire asks the Third Party to provide: 

- A SOC1, SOC2 Type 2 Report (or similar) and any applicable bridge letters. If there are any control deviations identified in that report, we also ask that they provide us with a status.
- A Penetration Test summary report and status of any findings or vulnerabilities.
- Uptime report for the prior 12 months
- Data flow diagram or information on where GitLab data will be stored
- Attestation of data desctruction policy
- Documentation on the Application's technical securtiy configurations

Third Parties who can not meet these standards will be asked to provide evidence of compensating controls through a more detailed questionnaire that is also sent through ZenGRC. 

### Third Parties Providing Services

**Professional Services Organizations, Resellers, Partners, Alliances, Mergers and Acquisitions**

- Third Party must sign GitLab's [Master Partner and SubContractor Agreement](https://gitlab.com/gitlab-com/www-gitlab-com/uploads/cd85eb8ec5191dbb9017ee4d37e85a7a/GitLab_Master_Partner_Training_and_Subcontract_Services_Agreement_April_2021.docx) or similar contract that includes a provision for confidentiality
- Third Party must provide information on their security and risk management practices and, if applicable, provide a copy of an annual independent audit (example: SOC1, SOC2 or similar) and any applicable bridge letters
- The third party must facilitate the implementation of identification and access management controls.
- The third party must securely manage passwords for users and devices. [See account and password standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/identification-and-authentication.html) 
- The third party must utilize the concept of least privilege, allowing only authorized access to processes necessary to accomplish assigned tasks in accordance with organizational business functions. [See least privilege policy](/handbook/engineering/security/access-management-policy.html#principle-of-least-privilege)
- The third party must detect and respond to anomalous behavior that could indicate account compromise or other malicious activities. [See endpoint security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/endpoint-security.html)
- The third party must utilize antimalware technologies to detect and eradicate malicious code. [See endpoint security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/endpoint-security.html)
- The third party must utilize File Integrity Monitor (FIM) technology to detect and report unauthorized changes to system files and configurations. [See endpoint security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/endpoint-security.html)
- The third party must utilize Host-based Intrusion Detection / Prevention Systems (HIDS / HIPS) on sensitive systems. [See endpoint security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/endpoint-security.html)
- The third party must utilize cryptographic mechanisms to protect the confidentiality of data being transmitted. [See encryption standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/cryptographic-protections.html)
- The third party must utilize cryptographic mechanisms on systems to prevent unauthorized disclosure of information at rest. [See encryption standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/cryptographic-protections.html)
- The third party must maintain network architecture diagrams that:
    - Contain sufficient detail to assess the security of the network's architecture;
    - Reflect the current state of the network environment; and
    - Document all sensitive data flows?
- The third party must employ mechanisms that:
    - Retain Personal Data (PD), including metadata, for an organization-defined time period to fulfill the purpose(s) identified in the notice or as required by law;
    - Disposes of, destroys, erases, and/or anonymizes the PI, regardless of the method of storage; and
    - Uses organization-defined techniques or methods to ensure secure deletion or destruction of PD (including originals, copies and archived records)?
- The third party must manage personnel security risk by screening individuals prior to authorizing access. [See Human Resources security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/human-resources-security.html)
- The third party must facilitate the implementation of security workforce development and awareness controls? [See security awareness training](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/security-awareness-and-training.html)
- The third party must require internal and third-party users to sign appropriate access agreements prior to being granted access?
- Third Party access requests must follow the [profesional services access request process](/handbook/business-technology/team-member-enablement/onboarding-access-requests/temporary-service-providers/). 

This information is collected by sending an automated questionnaire from ZenGRC. This questionnaire asks the Third Party to provide: 

- A SOC1, SOC2 Type 2 Report (or similar) and any applicable bridge letters. If there are any control deviations identified in that report, we also ask that they provide us with a status.
- Attestation that they can meet or exceed the minimum security standards listed on this page
- Information on where GitLab data will be stored

**Individual Contractors**

- The third party must detect and respond to anomalous behavior that could indicate account compromise or other malicious activities. [See endpoint security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/endpoint-security.html)
- The third party must utilize antimalware technologies to detect and eradicate malicious code. [See endpoint security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/endpoint-security.html)
- The third party must utilize File Integrity Monitor (FIM) technology to detect and report unauthorized changes to system files and configurations. [See endpoint security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/endpoint-security.html)
- The third party must utilize Host-based Intrusion Detection / Prevention Systems (HIDS / HIPS) on sensitive systems. [See endpoint security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/endpoint-security.html)
- The third party must utilize cryptographic mechanisms to protect the confidentiality of data being transmitted. [See encryption standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/cryptographic-protections.html)
- The third party must utilize cryptographic mechanisms on systems to prevent unauthorized disclosure of information at rest. [See encryption standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/cryptographic-protections.html)
- The third party must employ mechanisms that:
    - Retain Personal Data (PD), including metadata, for an organization-defined time period to fulfill the purpose(s) identified in the notice or as required by law;
    - Disposes of, destroys, erases, and/or anonymizes the PI, regardless of the method of storage; and
    - Uses organization-defined techniques or methods to ensure secure deletion or destruction of PD (including originals, copies and archived records)?
- The third party must monitor for evidence of unauthorized exfiltration or disclosure of organizational information.
- The third party must manage personnel security risk by screening individuals prior to authorizing access. [See Human Resources security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/human-resources-security.html)
- The third party must facilitate the implementation of security workforce development and awareness controls? [See security awareness training](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/security-awareness-and-training.html)
- The third party must require internal and third-party users to sign appropriate access agreements prior to being granted access?
- Third Party access requests must follow the [profesional services access request process](/handbook/finance/procurement/). 

**NOTE**: If the Contractor is being provided with a GitLab issued endpoint, no information is required from the Contractor to complete the Security Assessment. However, if the Contractor will be using their own device, an automated questionnaire is sent from ZenGRC. This questionnaire asks the Third Party to provide: 

- Attestation that they can meet or exceed the minimum security standards listed on this page
- Information on where GitLab data will be stored

### Third Parties Providing Physical Services (Field Marketing)

- Third Party must gain consent for any use of personal information
- The third party must employ mechanisms that:
    - Retain Personal Data (PD), including metadata, for an organization-defined time period to fulfill the purpose(s) identified in the notice or as required by law;
    - Disposes of, destroys, erases, and/or anonymizes the PI, regardless of the method of storage; and
    - Uses organization-defined techniques or methods to ensure secure deletion or destruction of PD (including originals, copies and archived records)?
- The third party must detect and respond to anomalous behavior that could indicate account compromise or other malicious activities. [See endpoint security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/endpoint-security.html)
- The third party must utilize antimalware technologies to detect and eradicate malicious code. [See endpoint security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/endpoint-security.html)
- The third party must utilize File Integrity Monitor (FIM) technology to detect and report unauthorized changes to system files and configurations. [See endpoint security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/endpoint-security.html)
- The third party must utilize Host-based Intrusion Detection / Prevention Systems (HIDS / HIPS) on sensitive systems. [See endpoint security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/endpoint-security.html)
- The third party must utilize cryptographic mechanisms to protect the confidentiality of data being transmitted. [See encryption standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/cryptographic-protections.html)
- The third party must utilize cryptographic mechanisms on systems to prevent unauthorized disclosure of information at rest. [See encryption standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/cryptographic-protections.html)
- Third Party must utilize a secured method for receiving personal data such as sharing through a document controlled by GitLab where access can be revoked once completed.


## Exceptions

1. GitLab may engage with a Professional Services Organization that provides a variety of services against multiple Statements of Work. In these cases, the Third Party will undergo a Security Assessment at the time of contract signing and annually thereafter. With each individual engagement, the contractors will follow the [procurement contractor process](/handbook/finance/procurement/#-step-7--create-professional-services-access-request-optional) and orientation process for [external consultants](/handbook/people-group/general-onboarding/consultants/). Security Assessments will not be conducted for each Statement of Work. 
1. If a Third Party does not provide the requested documents and/or completed questionnaires within 10 business days, each outstanding item will be rated based on [GitLab's Observation Risk Rating](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/observation-management.html#observation-risk-ratings) methodology and follow up activity will occur based on the risk. 
1.  If a Third Party cannot meet one of the standards noted on this page, and cannot provide any evidence of a countermeasure, each outstanding item will be rated based on [GitLab's Observation Risk Rating](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/observation-management.html#observation-risk-ratings) methodology and follow up activity will occur based on the risk. **NOTE** that in some cases, if the exception is a high enough risk to the organization, it will require a Risk Treatment plan through the [Security Operational Risk Management program](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-risk/storm-program/index.html).  

## SLA's

Once the requested information is received from the third party, we intend to have the audit completed within 10 business days.  The 10 business days start once the requested items are received.  This however, does not account for instances when there are delays in the Third Party responding to our follow up request.


## Guidance to sharing data externally

Before sending data **outside** of GitLab should review the following guidance:

<p>
<details markdown="1">
<Summary>Data Classification</summary>
<br>

Team members should review the [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html). For data that is classified as Yellow or above:

- The Third Party must first establish a non-disclosure agreement or similar confidentiality contractual clauses. See the guide and template for [GitLab confidentiality agreements](https://about.gitlab.com/handbook/finance/procurement/#contract-templates).
- Use the principle of "need-to-know" and obtain approval by the appropriate data owner. Good questions to ask yourself are:
  - What specific data does the third party need?
  - What can be omitted?
  - In what manner will they use the data?
  - Will they download it and share it with a fourth party?
  - Where will the data be stored?
  - How will they destroy it when they are done?
  - How will access be logged and monitored?
  - How will access be revoked?
  - How will access be reviewed to ensure it is still appropriate?
</details>

<p>
<details markdown="1">
<Summary>Consent for Use</summary>
<br>
For data that is classified as Yellow or above must have documented consent prior to sharing with Third Parties. Examples include:

- Customer Data (anything uploaded or created within GitLab by customers)
- Customer user analytics that is not anonymized
- Sending gifts to event attendees to a home address
- Team member personal information gathered during surveys
- Pictures and other identifying information of individuals

</details>

<p>
<details markdown="1">
<Summary>Methods of Sharing Data</summary>

For data that is classified as Yellow or above, the following guidelines should be used:

GitLab’s Tools | Instructions and Controls
|--| --|
| GitLab | - Create a new private project dedicated to the engagement with external users. <br> - Do not create an issue in an existing private project as the Guest level member will not be able to see the issue if private. <br> - Add the external user with Reporter permissions. <br> - Open an issue to share the non-public data in the description of the issue and mark it confidential. <br> - Do not attach documents with the data as the attachment can be accessed unauthenticated with the direct link. <br> - Include the disclaimer below in the description along with the data. <br> - Remove the user from the project once done and delete the issue.
| Google Workspace | - Create a dedicated folder for External Sharing and a subfolder for the external party. <br> - Upload the data to a document or sheet and verify the considerations in Step 2 before sending the invite to the external users. <br> - Include the disclaimer below in the document. <br> - Share the document to the intended party with the appropriate access level. <br> - Grant Editor access only if they need to collaborate in order to limit their ability to change permissions or invite others to the sheet. <br> - Viewer or Commenter access will allow the party to download, print or copy so this is the preference if they do not need to edit the document.
|Company email | - Consult the Security Assurance team to validate any additional steps that are needed for the type of data shared. <br> - Encrypt the document or sheet and password protect. An easy way to accomplish this is by creating a file and encrypting it with the Mac built-in [Disk Utility capability](https://www.macupdate.com/blog/post/66-how-do-you-encrypt-files-folders-on-mac).  <br> - Choose a random password generator with at least 60 characters. 1Password provides a [generator tool](https://1password.com/password-generator/) where you can adjust the length. <br> - Add the disclaimer below in the document or create a separate sheet in the workbook. <br> - When sending the encrypted file, Gmail may prompt you to store it in G-Drive if the size is over 25MB. You should allow this even if your recipient doesn't have a Google account; the direct link shouldn’t require it. <br> - Send the password in a separate email to the recipient outside a different channel than the encrypted document sent. For example, if the document was sent via email the password can be shared by a text message, phone call or a different email account. <br> - Ask the recipient to confirm deletion of the emails once successfully opened, and the document when no longer needed.|

</details>

<p>
<details markdown="1">
<Summary>Additional Tips</summary>
<br>

For data that is classified as Yellow or above:

- Do not store any local copies of data. If you have to download them, ensure you delete them as soon as possible.
- Do not use email to send data. If you receive an email containing these types of data, delete it and request it be sent using a secured method (see above).
- Create a mutual plan with timelines to return and/or dispose of the data once the Third Party no longer needs it. It might be helpful to schedule regular check-ins and identify deliverables that demonstrate expected progress.
- When the engagement is final, remove the user(s) from the document or sheet. Also, confirm with the external party that they have deleted any locally saved or shredded printed data.
- Utilize the below disclaimer below:

**Disclaimer**

>THIS DOCUMENT IS DISCLOSED ONLY TO THE RECIPIENT TO WHOM THIS DOCUMENT IS ADDRESSED AND IS PURSUANT TO A RELATIONSHIP OF CONFIDENTIALITY UNDER WHICH THE RECIPIENT HAS OBLIGATIONS OF CONFIDENTIALITY. THIS DOCUMENT MAY CONTAIN CONFIDENTIAL INFORMATION, PROPRIETARY INFORMATION AND/OR TRADE SECRETS (COLLECTIVELY “CONFIDENTIAL INFORMATION”) BELONGING TO GITLAB INC. AND/OR ITS AFFILIATES AND/OR SUBSIDIARIES. THE CONFIDENTIAL INFORMATION IS TO BE USED BY THE RECIPIENT ONLY FOR THE PURPOSE FOR WHICH THIS DOCUMENT IS SUPPLIED. THE CONTENTS OF THIS DOCUMENT ARE PROVIDED IN COMMERCIAL CONFIDENCE, SOLELY FOR THE PURPOSE, THIS DOCUMENT IS SUPPLIED TO THE RECIPIENT, BY GITLAB. ANY INFORMATION THAT GITLAB CONSIDERS TO BE A CONFIDENTIAL INFORMATION WILL NOT BE SUBJECT TO DISCLOSURE UNDER ANY PUBLIC RECORDS ACT. ANY DATA LOCALLY STORED MUST BE RETURNED AND/OR DISPOSED UPON COMPLETION OR NO LONGER NEEDED. PLEASE CONFIRM IN THIS DOCUMENT ONCE THE DISPOSAL IS DONE.

</details>

